﻿using System;
using System.Linq;
using WebApiCrud.Models;

namespace WebApiCrud.Tests
{
    class TestBookDbSet : TestDbSet<Book>
    {
        public override Book Find(params object[] keyValues)
        {
            return this.SingleOrDefault(book => book.Id == (int)keyValues.Single());
        }
    }
}
