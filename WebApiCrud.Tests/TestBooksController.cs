﻿using System;
using WebApiCrud.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Web.Http.Results;
using System.Collections.Generic;
using WebApiCrud.Controllers;
using System.Data.Entity;
using System.Net.Http;
using System.Web.Http;
using System.Net;

namespace WebApiCrud.Tests
{
    [TestClass]
    public class TestBooksController
    {       
        [TestMethod]
        public void PostBook_ShouldReturnSameBook()
        {
            var controller = new BooksController(new TestBookContext());

            var item = GetDemoBook();

            var result =
                controller.PostBook(item) as CreatedAtRouteNegotiatedContentResult<Book>;

            Assert.IsNotNull(result);
            Assert.AreEqual(result.RouteName, "DefaultApi");
            Assert.AreEqual(result.RouteValues["id"], result.Content.Id);
            Assert.AreEqual(result.Content.Name, item.Name);
        }

        [TestMethod]
        public void PutBook_ShouldReturnStatusCode()
        {
            var controller = new BooksController(new TestBookContext());

            var item = GetDemoBook();

            var result = controller.PutBook(item.Id, item) as StatusCodeResult;
            Assert.IsNotNull(result);
            Assert.IsInstanceOfType(result, typeof(StatusCodeResult));
            Assert.AreEqual(HttpStatusCode.NoContent, result.StatusCode);
        }

        [TestMethod]
        public void PutBook_ShouldFail_WhenDifferentID()
        {
            var controller = new BooksController(new TestBookContext());

            var badresult = controller.PutBook(999, GetDemoBook());
            Assert.IsInstanceOfType(badresult, typeof(BadRequestResult));
        }

        [TestMethod]
        public void GetBook_ShouldReturnBookWithSameID()
        {
            var context = new TestBookContext();
            context.Books.Add(GetDemoBook());

            var controller = new BooksController(context);
            var result = controller.GetBook(3) as OkNegotiatedContentResult<Book>;

            Assert.IsNotNull(result);
            Assert.AreEqual(3, result.Content.Id);
        }

        [TestMethod]
        public void DeleteBook_ShouldReturnOK()
        {
            var context = new TestBookContext();
            var item = GetDemoBook();
            context.Books.Add(item);

            var controller = new BooksController(context);
            var result = controller.DeleteBook(3) as OkNegotiatedContentResult<Book>;

            Assert.IsNotNull(result);
            Assert.AreEqual(item.Id, result.Content.Id);
        }

        Book GetDemoBook()
        {
            return new Book() { Id = 3, Name = "Demo name" };
        }
    }
}
