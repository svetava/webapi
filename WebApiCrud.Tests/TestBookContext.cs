﻿using System;
using WebApiCrud.Models;
using System.Data.Entity;

namespace WebApiCrud.Tests
{
    public class TestBookContext : IBookContext
    {
        public TestBookContext()
        {
            this.Books = new TestBookDbSet();
        }

        public DbSet<Book> Books { get; set; }
        public DbSet<Author> Authors { get; set; }

        public int SaveChanges()
        {
            return 0;
        }

        public void MarkAsModified(Book item) { }

        public void MarkAsModified(Author item) { }

        public void GetEntry(Book book) { }

        public void Dispose() { }
    }
}
