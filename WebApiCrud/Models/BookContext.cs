﻿using System;
using System.Data.Entity;

namespace WebApiCrud.Models
{
    public class BookContext : DbContext, IBookContext
    {
        public DbSet<Book> Books { get; set; }
        public DbSet<Author> Authors { get; set; }

        public void MarkAsModified(Book item)
        {
            Entry(item).State = EntityState.Modified;
        }

        public void MarkAsModified(Author item)
        {
            Entry(item).State = EntityState.Modified;
        }

        public void GetEntry(Book book)
        {
            Entry(book).Reference(b => b.Author).Load();
        }
    }
}