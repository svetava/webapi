﻿using System;
using System.Data.Entity;

namespace WebApiCrud.Models
{
    public interface IBookContext : IDisposable
    {
        DbSet<Book> Books { get; }
        DbSet<Author> Authors { get; }
        int SaveChanges();
        void MarkAsModified(Author item);
        void MarkAsModified(Book item);
        void GetEntry(Book book);
    }
}
